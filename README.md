# Graph2D C++简单图形库Code::Blocks Wizard

#### 项目介绍

该项目是为西北农林科技大学信息工程学院计算机科学系胡少军老师编写的`Graph2D`简单图形库在`Code::Blocks`开发图形应用程序时提供一个`Wizard`(向导)，以简化图形库的使用配置。

#### 项目介绍

1. 将`Graph2D`文件夹和`config.script`复制到`CodeBlocks`的安装目录中的`“share\CodeBlocks\templates\wizard”`文件夹中，如：
```
   C:\Program Files (x86)\CodeBlocks\share\CodeBlocks\templates\wizard
```
   *注意*：本`config.script`中，注释了不需要的`Wizard`，如果需要，请打开该文件取消对应的注释。

2. 将`devLibs`文件夹及其中所有文件**保持目录结构**拷贝到任意位置。

3. 通过`Code::Blocks`的`Settings->Global variables...`设置`cppg2d`和`freeglut`两个全局环境变量，如：

<p align="center">
<img src="./screenshot/cppg2d.png"/>
<img src="./screenshot/freeglut.png"/>
</p>

   *注意*：请将`Build-in fields:`中的`base`、`include`、`lib`、`bin`**修改为`devLibs`文件夹所在的路径**。

4. 使用`Create a new project`打开新建工程向导窗口，选择`Graph2D application`即可生成`Graph2D`简单图形库程序代码框架并配置相关编译、链接和运行参数，如：

<p align="center">
<img src="./screenshot/cppg2dwizard.png"/>
</p>

#### 项目维护
  `Graph2D`简单图形库源码见：https://github.com/DillonHu/Graph2DLibForCPP

  为便于维护，在本项目中也复制了`Graph2D`简单图形库源码，见`SRC`文件夹。
  
  **胡少军** <hsj@nwsuaf.edu.cn>

  **耿楠** <nangeng@nwsuaf.edu.cn>
